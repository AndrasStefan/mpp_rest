package triatlon.model;

public class Proba {
    private int id;

    private String descriere;
    private int id_arbitru;

    public Proba() { }

    public Proba(int id, String descriere, int id_arbitru) {
        this.id = id;
        this.descriere = descriere;
        this.id_arbitru = id_arbitru;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescriere() {
        return descriere;
    }

    public void setDescriere(String descriere) {
        this.descriere = descriere;
    }

    public int getId_arbitru() {
        return id_arbitru;
    }

    public void setId_arbitru(int id_arbitru) {
        this.id_arbitru = id_arbitru;
    }

    @Override
    public String toString() {
        return "Proba{" +
                "id=" + id +
                ", descriere='" + descriere + '\'' +
                ", id_arbitru=" + id_arbitru +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Proba proba = (Proba) o;

        if (id != proba.id) return false;
        if (id_arbitru != proba.id_arbitru) return false;
        return descriere != null ? descriere.equals(proba.descriere) : proba.descriere == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (descriere != null ? descriere.hashCode() : 0);
        result = 31 * result + id_arbitru;
        return result;
    }
}
