package triatlon.model;

public class Arbitru {
    private int id;
    private String nume;
    private String parola;

    public Arbitru() {}

    public Arbitru(int id, String nume, String parola) {
        this.id = id;
        this.nume = nume;
        this.parola = parola;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getParola() {
        return parola;
    }

    public void setParola(String parola) {
        this.parola = parola;
    }

    @Override
    public String toString() {
        return "Arbitru{" +
                "id=" + id +
                ", nume='" + nume + '\'' +
                ", parola='" + parola + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Arbitru arbitru = (Arbitru) o;

        if (id != arbitru.id) return false;
        if (nume != null ? !nume.equals(arbitru.nume) : arbitru.nume != null) return false;
        return parola != null ? parola.equals(arbitru.parola) : arbitru.parola == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (nume != null ? nume.hashCode() : 0);
        result = 31 * result + (parola != null ? parola.hashCode() : 0);
        return result;
    }
}
