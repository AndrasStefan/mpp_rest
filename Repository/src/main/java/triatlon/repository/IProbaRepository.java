package triatlon.repository;

import triatlon.model.Arbitru;
import triatlon.model.Proba;

public interface IProbaRepository extends ICrudRepository<Integer, Proba> {
    Iterable<Proba> getAllByArb(Integer idArb);
    Iterable<Arbitru> getAllArbitrii();
    Arbitru findOneArb(Integer idArb);
    Proba findOneWithArb(Integer idProba, Integer idArb);
}
