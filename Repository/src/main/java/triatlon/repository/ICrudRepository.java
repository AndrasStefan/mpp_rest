package triatlon.repository;

public interface ICrudRepository<ID, E> {
    void save(E e);
    void delete(ID id);
    E findOne(ID id);
    void update(ID id, E e);
    Iterable<E> getAll();
}
