package triatlon.repository;

import org.springframework.stereotype.Component;
import triatlon.model.Arbitru;
import triatlon.model.Proba;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


@Component
public class ProbaRepository implements IProbaRepository {
    private Map<Integer, Proba> allProbe;
    private Map<Integer, Arbitru> allArbitrii;

    public ProbaRepository() {
        this.allProbe = new TreeMap<>();
        this.allArbitrii = new TreeMap<>();
        initialData();
    }

    private void initialData() {
        Arbitru arb1 = new Arbitru(1, "Adrian", "123");
        Arbitru arb2 = new Arbitru(2, "Andrei", "1997");
        allArbitrii.put(arb1.getId(), arb1);
        allArbitrii.put(arb2.getId(), arb2);

        Proba prob1 = new Proba(1, "Natatie", 1);
        Proba prob2 = new Proba(2, "Ciclism", 1);
        Proba prob3 = new Proba(3, "Alergare, distanta 5 km", 2);
        Proba prob4 = new Proba(4, "Alergare, distanta 10 km", 2);
        allProbe.put(prob1.getId(), prob1);
        allProbe.put(prob2.getId(), prob2);
        allProbe.put(prob3.getId(), prob3);
        allProbe.put(prob4.getId(), prob4);
    }

    @Override
    public void save(Proba proba) {
        if (allProbe.containsKey(proba.getId()))
            throw new RepositoryException("[SAVE] Proba already exists: " + proba.getId());

        if (!allArbitrii.containsKey(proba.getId_arbitru()))
            throw new RepositoryException("[SAVE] Arbitru with id = " + proba.getId_arbitru() + " doesn't exist");

        allProbe.put(proba.getId(), proba);
    }

    @Override
    public void delete(Integer integer) {
        if (allProbe.containsKey(integer))
            allProbe.remove(integer);
        else
            throw new RepositoryException("[DELETE] Proba with id = " + integer + " doesn't exist");
    }

    @Override
    public Proba findOne(Integer integer) {
        Proba prb = allProbe.get(integer);
        if (prb == null)
            throw new RepositoryException("[FIND ONE] Proba with id = " + integer + " doesn't exist");
        return prb;
    }

    @Override
    public void update(Integer integer, Proba proba) {
        Proba prb = allProbe.get(integer);

        if (prb == null)
            throw new RepositoryException("[UPDATE] Proba with id = " + integer + " doesn't exist");

        if (proba.getDescriere() != null)
            prb.setDescriere(proba.getDescriere());

        if (proba.getId_arbitru() != 0) {
            Arbitru arb = allArbitrii.get(proba.getId_arbitru());

            if (arb == null)
                throw new RepositoryException("[UPDATE] Arbitru with id = " + integer + " doesn't exist");

            prb.setId_arbitru(proba.getId_arbitru());
        }

        allProbe.put(integer, prb);
    }

    @Override
    public Iterable<Proba> getAll() {
        return allProbe.values();
    }

    @Override
    public Iterable<Proba> getAllByArb(Integer idArb) {
        List<Proba> rez = new ArrayList<>();

        for (Proba prb : allProbe.values()) {
            if (prb.getId_arbitru() == idArb) {
                rez.add(prb);
            }
        }

        if (rez.size() == 0)
            throw new RepositoryException("Nope");

        return rez;
    }

    @Override
    public Iterable<Arbitru> getAllArbitrii() {
        return allArbitrii.values();
    }

    @Override
    public Arbitru findOneArb(Integer idArb) {
        Arbitru arb = allArbitrii.get(idArb);
        if (arb == null)
            throw new RepositoryException("[FIND ONE ARB] Arbitru with id = " + idArb + " doesn't exist");
        return arb;
    }


    @Override
    public Proba findOneWithArb(Integer idProba, Integer idArb) {
        Proba prb = allProbe.get(idProba);
        if (prb == null || prb.getId_arbitru() != idArb)
            throw new RepositoryException("[FIND ONE WITH ARB] Proba with id = " + idProba + " doesn't belong to: " +idArb);
        return prb;
    }
}
