package triatlon.services.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import triatlon.model.Proba;
import triatlon.repository.IProbaRepository;

/*
URL mapping
GET /triatlon/arbitrii - toti arbitrii
GET /triatlon/probe - toate problele

GET /triatlon/probe/id     - ia proba
DELETE /triatlon/probe/id  - sterge proba
PUT /triatlon/probe/id     - update pe proba cu id

POST /triatlon/arbitrii/id/probe - creeaza proba la arbitru cu id
GET /triatlon/arbitrii/id/probe - toate probele unui arbitru

GET /triatlon/arbitrii/id/probe/id - o singura proba a unui arbitru
 */

@RestController
@RequestMapping("/triatlon")
public class ProbaController {

    @Autowired
    private IProbaRepository probaRepository;

    @RequestMapping(value = "/probe/{id}", method = RequestMethod.GET)
    public Proba getById(@PathVariable Integer id) {
        return probaRepository.findOne(id);
    }

    @RequestMapping(value = "/probe/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        probaRepository.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/probe/{id}", method = RequestMethod.PUT)
    public Proba update(@RequestBody Proba proba, @PathVariable Integer id) {
        probaRepository.update(id, proba);
        return probaRepository.findOne(id);
    }


    @RequestMapping(value = "/probe", method = RequestMethod.GET)
    public ResponseEntity<?> getAll() {
        return new ResponseEntity<>(probaRepository.getAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "/arbitrii", method = RequestMethod.GET)
    public ResponseEntity<?> getAllArbs() {
        return new ResponseEntity<>(probaRepository.getAllArbitrii(), HttpStatus.OK);
    }

    @RequestMapping(value = "/arbitrii/{id}/probe", method = RequestMethod.GET)
    public ResponseEntity<?> getAllByArb(@PathVariable Integer id) {
        return new ResponseEntity<>(probaRepository.getAllByArb(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/arbitrii/{id}/probe", method = RequestMethod.POST)
    public Proba add(@RequestBody Proba proba, @PathVariable Integer id) {
        proba.setId_arbitru(id);
        probaRepository.save(proba);
        return proba;
    }

    @RequestMapping(value = "/arbitrii/{idArb}/probe/{idProba}", method = RequestMethod.GET)
    public Proba getByIdAndArb(@PathVariable Integer idArb, @PathVariable Integer idProba) {
        return probaRepository.findOneWithArb(idProba, idArb);
    }
}
