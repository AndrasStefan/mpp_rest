package start;

import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import triatlon.model.Arbitru;
import triatlon.model.Proba;

public class StartRestClient {

    private static void print(Object message) {
        final String BLUE_BOLD = "\033[1;34m";
        final String RESET = "\033[0m";

        System.out.println(BLUE_BOLD + message.toString() + RESET);
    }

    private static void printMethodname(Object message) {
        String YELLOW_BOLD = "\033[1;33m";
        final String RESET = "\033[0m";

        System.out.println(YELLOW_BOLD + message.toString() + RESET);
    }

    private static void printError(Object message) {
        final String RED_BOLD = "\033[1;31m";
        final String RESET = "\033[0m";

        System.out.println(RED_BOLD + message.toString() + RESET);
    }

    private static void getProbe(RestTemplate rest, String URL) {
        printMethodname("***** " + Thread.currentThread().getStackTrace()[1].getMethodName() +" *****");

        Proba[] all = rest.getForObject(URL, Proba[].class);
        for (Proba proba : all) {
            print(proba);
        }
    }

    private static void getArbitrii(RestTemplate rest, String URL) {
        printMethodname("***** " + Thread.currentThread().getStackTrace()[1].getMethodName() + " *****");

        Arbitru[] all = rest.getForObject(URL, Arbitru[].class);
        for (Arbitru arb : all) {
            print(arb);
        }
    }

    private static void getProbaById(RestTemplate rest, String URL) {
        printMethodname("***** " + Thread.currentThread().getStackTrace()[1].getMethodName() + " *****");

        try {
            Proba prb = rest.getForObject(String.format("%s/%s", URL, 1), Proba.class);
            print(prb);
            Proba prb1 = rest.getForObject(String.format("%s/%s", URL, 5), Proba.class);
        } catch (RestClientException ex) {
            printError(ex);
        }
    }

    private static void deleteProba(RestTemplate rest, String URL) {
        printMethodname("***** " + Thread.currentThread().getStackTrace()[1].getMethodName() + " *****");
        rest.delete(String.format("%s/%s", URL, 10));
    }

    private static void addProba(RestTemplate rest, String URL) {
        printMethodname("***** " + Thread.currentThread().getStackTrace()[1].getMethodName() + " *****");

        try {
            Proba prb = new Proba(10, "cea mai noua proba", 2);
            Proba newprb = rest.postForObject(String.format("%s/%s/%s", URL, 1, "probe"), prb, Proba.class);
            print(newprb);

            Proba prb1 = new Proba(10, "cea mai noua proba", 214);
            rest.postForObject(String.format("%s/%s/%s", URL, 40, "probe"), prb, Proba.class);
        } catch (RestClientException ex) {
            printError(ex);
        }
    }

    private static void updateProba(RestTemplate rest, String URL) {
        printMethodname("***** " + Thread.currentThread().getStackTrace()[1].getMethodName() + " *****");

        Proba prb = new Proba(123, "din update", 0);
        rest.put(String.format("%s/%s", URL, 10), prb, Proba.class);
    }

    public static void main(String[] args) {
        String URLprob = "http://localhost:8080/triatlon/probe";
        String URLarb = "http://localhost:8080/triatlon/arbitrii";
        RestTemplate rest = new RestTemplate();

        getProbe(rest, URLprob);
        getArbitrii(rest, URLarb);
        getProbaById(rest, URLprob);

        addProba(rest, URLarb);
        getProbe(rest, URLprob);

        updateProba(rest, URLprob);
        getProbe(rest, URLprob);

        deleteProba(rest, URLprob);
        getProbe(rest, URLprob);
    }
}
